package com.classpath.paymentmicroservice.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PaymentProcessor {

    @KafkaListener(topics = "orders-topic")
    public void processOrder(String order){
      log.info("Processing the order :: {}", order);
    }
}